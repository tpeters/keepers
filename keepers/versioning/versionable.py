# -*- coding: utf-8 -*-

# from builtins import str
from builtins import object
import numpy as np

from keepers.keepers_dependency_injector import keepers_dependency_injector

try:
    MPI = keepers_dependency_injector['MPI']
except KeyError:
    MPI = keepers_dependency_injector['MPI_dummy']


class Versionable(object):
    def _get_versioning_id(self):
        if '_global_id' not in self.__dict__:
            if self._versioning_comm.rank == 0:
                global_id = self.__class__.__name__ + '_' + \
                            str(int(id(self) * np.random.rand()))
            else:
                global_id = None

            global_id = self._versioning_comm.bcast(global_id, root=0)
            self._global_id = global_id
        return self._global_id

    @property
    def _versioning_comm(self):
        return MPI.COMM_WORLD

    def _to_hdf5(self, hdf5_group):
        """
        If the Versionable contains objects that also should be saved,
        return a dictionary with them and the keys as there desired names.
        """
        raise NotImplemented

    @classmethod
    def _from_hdf5(cls, hdf5_group, repository):
        """
        If the Versionable shall be composed by other Versionables, use the
        method
        repository.get(name_of_desired_object, hdf5_group)
        """
        raise NotImplemented
