# -*- coding: utf-8 -*-

from builtins import str
from builtins import object
import sys

import logging

from keepers.keepers_dependency_injector import keepers_dependency_injector

from keepers.configuration import Variable,\
                                  ConfigurationManager
from future.utils import with_metaclass

try:
    MPI = keepers_dependency_injector['MPI']
except KeyError:
    MPI = keepers_dependency_injector['MPI_dummy']

loglevels = ['CRITICAL', 'ERROR', 'WARNING', 'INFO', 'DEBUG']

variable_ignore_high_MPI_ranks = Variable('ignore_high_MPI_ranks',
                                          [True],
                                          lambda z: isinstance(z, bool),
                                          genus='boolean')

variable_log_to_console = Variable('log_to_console',
                                   [True],
                                   lambda z: isinstance(z, bool),
                                   genus='boolean')

variable_console_log_level = Variable('console_log_level',
                                      ['WARNING'],
                                      lambda z: z in loglevels,
                                      genus='str')


variable_log_to_disk = Variable('log_to_disk',
                                [True, False],
                                lambda z: isinstance(z, bool),
                                genus='boolean')

variable_disk_log_level = Variable('disk_log_level',
                                   ['DEBUG'],
                                   lambda z: z in loglevels,
                                   genus='str')

variable_log_file_path = Variable('log_file_path',
                                  ['./log.log'],
                                  lambda z: str(z) == z,
                                  genus='str')


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = \
                super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class MPILogger(with_metaclass(Singleton, object)):
    def __init__(self, configuration_file_name='logger.conf',
                 configuration_search_pathes=None,
                 comm=MPI.COMM_WORLD):
        self.comm = comm
        self.conf = ConfigurationManager.get_Configuration(
                                 name='MPILogger',
                                 file_name=configuration_file_name,
                                 variables=[variable_ignore_high_MPI_ranks,
                                            variable_log_to_console,
                                            variable_console_log_level,
                                            variable_log_to_disk,
                                            variable_disk_log_level,
                                            variable_log_file_path],
                                 search_paths=configuration_search_pathes)

        self.logger = logging.getLogger()
        self.logger.setLevel(logging.NOTSET)
        self.handlers = []

        if self.comm.rank == 0 or not self.conf['ignore_high_MPI_ranks']:
            # create file handler
            if self.conf['log_to_disk']:
                log_file_path = self.conf['log_file_path']
                if self.comm.size > 1:
                    log_file_path += ('_' + str(self.comm.rank))

                fh = logging.FileHandler(log_file_path)
                disk_log_level = eval('logging.' + self.conf['disk_log_level'])
                fh.setLevel(disk_log_level)
                # create formatters and add them to the handlers
                file_formatter = logging.Formatter(
                   fmt='[%(asctime)s][%(threadName)-10s][%(levelname)-8s] '
                       '%(name)-12s %(message)s',
                   datefmt='%m-%d %H:%M:%S')
                fh.setFormatter(file_formatter)
                self.handlers += [fh]

            # create console handler
            if self.conf['log_to_console']:
                ch = logging.StreamHandler()
                console_log_level = eval('logging.' +
                                         self.conf['console_log_level'])
                ch.setLevel(console_log_level)

                stream_formatter = logging.Formatter(
                    fmt='[%(threadName)-10s][%(levelname)-8s] '
                        '%(name)-12s %(message)s')
                ch.setFormatter(stream_formatter)
                self.console_handler = ch
                self.handlers += [ch]
        else:
            self.handlers += [logging.NullHandler()]

        for handler in self.handlers:
            self.logger.addHandler(handler)

        # hook into the excepthook in order to log uncaught exceptions
        sys.excepthook = self._handle_exception

    def _handle_exception(self, exc_type, exc_value, exc_traceback):
        if issubclass(exc_type, KeyboardInterrupt):
            sys.__excepthook__(exc_type, exc_value, exc_traceback)
            return
        self.logger.error("Uncaught exception",
                          exc_info=(exc_type, exc_value, exc_traceback))
