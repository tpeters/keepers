# -*- coding: utf-8 -*-

from __future__ import absolute_import
from builtins import object
from .configuration import Configuration


class _ConfigurationManager(object):
    def __init__(self):
        self.configurations_dictionary = {}

    def get_Configuration(self, name, *args, **kwargs):
        if name not in self.configurations_dictionary:
            new_configuration = Configuration(*args, **kwargs)
            self.configurations_dictionary[name] = new_configuration
        return self.configurations_dictionary[name]

ConfigurationManager = _ConfigurationManager()
