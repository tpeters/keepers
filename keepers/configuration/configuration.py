# -*- coding: utf-8 -*-

from future import standard_library
standard_library.install_aliases()
from builtins import map
from builtins import object
import os
import configparser
import pprint


class Configuration(object):
    """
    configuration
    init with file path to configuration file
    get command -> dictionary like
    set command -> parse input for sanity
    reset -> reset to defaults
    load from file
    save to file
    """
    def __init__(self, file_name=None, search_paths=None,
                 section='DEFAULT', variables=[]):
        self.variable_dict = {}
        list(map(self.register, variables))

        self.file_name = str(file_name)

        if search_paths is None:
            search_paths = [os.path.expanduser('~/.config'),
                            '.']
        self.search_paths = search_paths

        self.section = str(section)

        try:
            self.load()
        except:
            pass

    def __getitem__(self, key):
        return self.get_variable(key)

    def __setitem__(self, key, value):
        return self.set_variable(key, value)

    def register(self, variable):
        self.variable_dict[variable.get_name()] = variable

    def get_variable(self, name):
        try:
            return self.variable_dict[name].get_value()
        except KeyError:
            raise KeyError("The requested variable is not registered!")

    def set_variable(self, name, value):
        try:
            return self.variable_dict[name].set_value(value)
        except KeyError:
            raise KeyError("The requested variable is not registered!")

    def reset(self):
        for key, item in list(self.variable_dict.items()):
            item.set_value(None)

    def validQ(self, name, value):
        return self.variable_dict[name].checker(value)

    def save(self, path=None, section=None):
        if path is None:
            path = os.path.join(self.search_paths[-1], self.file_name)
        else:
            path = str(path)

        if section is None:
            section = self.section

        config_parser = configparser.ConfigParser()
        try:
            config_parser.add_section(section)
        except ValueError:
            pass

        for item in self.variable_dict:
            config_parser.set(section,
                              item,
                              str(self[item]))
        # create directory if it does not exist
        directory = os.path.dirname(path)
        if not os.path.exists(directory):
                os.makedirs(directory)

        # write config file
        config_file = open(path, 'wb+')
        config_parser.write(config_file)

    def load(self, path=None, section=None):
        if path is None:
            path_list = [os.path.join(p, self.file_name)
                         for p in self.search_paths]
        else:
            path_list = [str(path)]

        if section is None:
            section = self.section

        config_parser = configparser.ConfigParser()
        config_parser.read(path_list)

        for key, item in list(self.variable_dict.items()):
            if item.genus == 'str':
                temp_value = str(config_parser.get(section, item.name))
            elif item.genus == 'int':
                temp_value = config_parser.getint(section, item.name)
            elif item.genus == 'float':
                temp_value = config_parser.getfloat(section, item.name)
            elif item.genus == 'boolean':
                temp_value = config_parser.getboolean(section, item.name)
            else:
                raise ValueError("Unknown variable genus.")

            item.set_value(temp_value)

    def __repr__(self):
        return_string = ("<Configuration> \n" +
                         pprint.pformat(self.variable_dict))
        return return_string
