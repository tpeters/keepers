# -*- coding: utf-8 -*-

from builtins import object


class Parametrization(object):
    def __init__(self, parameters=[]):
        self.parameters = {}
        for p in parameters:
            self.parameters[p.name] = p

    def get(self, name, itag=None):
        return self.parameters[name](itag)
