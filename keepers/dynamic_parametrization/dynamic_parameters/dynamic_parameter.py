# -*- coding: utf-8 -*-

from builtins import str
from builtins import object
from ..call_tag import CALL_TAG


class DynamicParameter(object):
    call_tag = CALL_TAG

    def __init__(self, name):
        self.name = str(name)

    def __call__(self, itag=None):
        raise NotImplementedError
