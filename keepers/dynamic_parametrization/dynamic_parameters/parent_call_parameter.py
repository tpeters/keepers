# -*- coding: utf-8 -*-

from .dynamic_parameter import DynamicParameter


class ParentCallParameter(DynamicParameter):
    def __init__(self, name, rules=[]):
        """
        rules must be a list containing lists of length==3 with
        [last_call_tag, itag, value]
        """
        super(ParentCallParameter, self).__init__(name)
        self.rules = rules

    def __call__(self, itag=None):
        # iterate through the rules and find first match
        ctag = self.call_tag

        for rule in self.rules:
            if rule[0] is not None:
                if len(ctag) == 0:
                    continue
                elif ctag[-1] != rule[0]:
                    continue

            if rule[1] is not None:
                if itag is None or itag != rule[1]:
                    continue

            return rule[2]

        raise ValueError("No matching parameter value found.")
